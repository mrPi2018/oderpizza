package hoabd.aprotrain.pizzafood.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import hoabd.aprotrain.pizzafood.Food;
import hoabd.aprotrain.pizzafood.R;

public class CustomAdapter extends BaseAdapter {
    Activity activity;
    List<Food> dataList;

    public CustomAdapter(Activity activity, List<Food> dataList){
        this.activity = activity;
        this.dataList =dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      //  convertView = LayoutInflater.from(activity).inflate(R.layout.food_item, parent, false);
        View view = activity.getLayoutInflater().inflate(R.layout.food_item, parent, false);
        ImageView thumbnail = view.findViewById(R.id.ft_thumbnail);
        TextView title = view.findViewById(R.id.ft_title);
        TextView description = view.findViewById(R.id.ft_description);
        TextView price = view.findViewById(R.id.ft_price);

        Food food = dataList.get(position);

        thumbnail.setImageResource(food.getImageId());
        title.setText(food.getTitle());
        description.setText(food.getDescription());
        price.setText(String.valueOf(food.getPrice()));

        return view;
    }
}
