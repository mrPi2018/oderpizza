package hoabd.aprotrain.pizzafood;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ItemActivity extends AppCompatActivity {
    EditText editTitle, editDescription, editPrice;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        editTitle = findViewById(R.id.ai_title);
        editDescription = findViewById(R.id.ai_description);
        editPrice= findViewById(R.id.ai_price);
        btnSave = findViewById(R.id.ai_btnAdd);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = editTitle.getText().toString();
                String des = editDescription.getText().toString();
                String price = editPrice.getText().toString();

                //chuyen du lieu nay ve FoodActivity
                Intent intent = new Intent();
                intent.putExtra("title",title);
                intent.putExtra("des", des);
                intent.putExtra("price",price);

               setResult(100,intent);
              // onActivityResult(100,200,intent);
                finish();
            }
        });

    }
}
