package hoabd.aprotrain.pizzafood;

public class Food {
    String title, description;
    float price;
    int imageId;

    public Food() {
    }

    public Food(String title, String description, float price, int imageId) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
