package hoabd.aprotrain.pizzafood;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hoabd.aprotrain.pizzafood.adapter.CustomAdapter;

public class FoodActivity extends AppCompatActivity {
    ListView foodListView;
    List<Food> FoodDataListView = new ArrayList<>();
    CustomAdapter foodAdapter;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        foodListView = findViewById(R.id.foodListView);
        Food food = new Food("Pizza01", "Mo ta pizza01", 5.25f, R.drawable.image01);
        FoodDataListView.add(food);

        food = new Food("Pizza02", "Mo ta pizza02", 5.25f, R.drawable.image02);
        FoodDataListView.add(food);

        food = new Food("Pizza03", "Mo ta pizza03", 5.25f, R.drawable.image03);
        FoodDataListView.add(food);

        food = new Food("Pizza04", "Mo ta pizza04", 5.25f, R.drawable.image04);
        FoodDataListView.add(food);

        food = new Food("Pizza05", "Mo ta pizza05", 5.25f, R.drawable.image05);
        FoodDataListView.add(food);

        foodAdapter = new CustomAdapter(this, FoodDataListView);
        //Chuyen adapter vao listView
        foodListView.setAdapter(foodAdapter);
//Cap nhat lai du lieu listView -> khi data thay doi
        foodAdapter.notifyDataSetChanged();
        //Dang ky bat su kien khi click item cua ListView
        foodListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Food f = FoodDataListView.get(position);
                Toast.makeText(FoodActivity.this, f.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
// Xu ly button add
        btnAdd = findViewById(R.id.btn_addProduct);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show len activity -> cho phep them san pham moi
                //tao mot activity cho phep add/update san pham

                Intent intent = new Intent(FoodActivity.this, ItemActivity.class);
             startActivityForResult(intent, 100);

            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String title = data.getStringExtra("title");
        String des = data.getStringExtra("des");
        String priceStr = data.getStringExtra("price");
        float price = Float.parseFloat(priceStr);

        //Random hinh anh
        int thumnail = R.drawable.image01;

        Food food = new Food(title, des, price, thumnail);
        FoodDataListView.add(food);

        //update du lieu tren listView
        foodAdapter.notifyDataSetChanged();

    }
}
